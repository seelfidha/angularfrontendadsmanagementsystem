import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContentComponent } from './components/content/content.component';
import { NavBarComponent } from './components/menu/nav-bar/nav-bar.component';
import { SideBarComponent } from './components/menu/side-bar/side-bar.component';
import { CategoryComponent } from './components/content/category/category/category.component';
import { CategoryService } from './services/category/category.service';
import { PaymentModeService } from './services/paymentMode/payment-mode.service';
import { PaymentModeComponent } from './components/content/paymentMode/payment-mode/payment-mode.component';
import { AdComponent } from './components/content/ad/ad/ad.component';
import { AdFormComponent } from './components/content/ad/ad-form/ad-form.component';
import { AdService } from './services/ad/ad.service';
import { RouterModule } from '@angular/router';
import { AdListComponent } from './components/content/ad/ad-list/ad-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ContentComponent,
    NavBarComponent,
    SideBarComponent,
    CategoryComponent,
    PaymentModeComponent,
    AdComponent,
    AdFormComponent,
    AdListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [CategoryService, PaymentModeService,AdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
