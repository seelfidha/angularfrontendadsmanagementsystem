import { TestBed } from '@angular/core/testing';

import { PaymentModeService } from './payment-mode.service';

describe('PaymentModeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentModeService = TestBed.get(PaymentModeService);
    expect(service).toBeTruthy();
  });
});
