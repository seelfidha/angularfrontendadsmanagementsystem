import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentMode } from 'src/app/entities/paymentMode/payment-mode';

@Injectable({
  providedIn: 'root'
})
export class PaymentModeService {

  baseUrl : string = 'http://localhost:9090/api/paymentModes';
  headers : Headers = new Headers({'content-type':'application/JSON'});

  constructor(private httpClient:HttpClient) { }

  findAllPaymentModes() : Observable<PaymentMode[]>{

    return this.httpClient.get<PaymentMode[]>(this.baseUrl+"/all");
  }

  savePaymentMode(paymentMode:PaymentMode) : Observable<any>{
    return this.httpClient.post(this.baseUrl+"/save", paymentMode);
  }

  updatePaymentMode(paymentMode:PaymentMode) : Observable<any>{
    return this.httpClient.put(this.baseUrl+"/update",paymentMode);
  }

  deletePaymentMode(idPaymentMode:number) : Observable<any>{
    return this.httpClient.delete(this.baseUrl+"/delete/"+idPaymentMode);
  }
}
