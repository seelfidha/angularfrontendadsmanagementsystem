import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ad } from 'src/app/entities/ad/ad';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdService {

  baseUrl : string ='http://localhost:9090/api/ads';
  headers : Headers = new Headers({'content-type':'application/JSON'});

  selectedAd:Ad;
  ads:Ad[] = [];

  constructor(private httpClient:HttpClient) { }

  setSelectedAd(ad:Ad){
    this.selectedAd = ad;
  }

  getSelectedAd():Ad{
    return this.selectedAd;
  }

  findAllAds():Observable<Ad[]>{
    return this.httpClient.get<Ad[]>(this.baseUrl+"/all");
  }

  findByID(idAd:number):Observable<any>{
    return this.httpClient.get<Ad>(this.baseUrl+"/one"+idAd);
  }

  saveAd(ad:Ad):Observable<any>{
    
    console.log(ad);
    return this.httpClient.post<Ad>(this.baseUrl+"/save",ad,{
                                    headers:{
                                      'content-type':"application/json"}
                                    });
    console.log(ad.name+" "+ad.description);
  }
}
