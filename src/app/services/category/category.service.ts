import { Injectable, ɵlooseIdentical } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Category } from 'src/app/entities/category/category';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseUrl = 'http://localhost:9090/api/categories';
  headers:Headers = new Headers({'content-type':'application/JSON'});

  categories : Category[]=[];

  constructor(private httpClient: HttpClient) { }

  findAllCategories() : Observable<Category[]>{
    return this.httpClient.get<Category[]>(this.baseUrl+"/all");
  }

  saveCategory(category:Category) : Observable<any>{
    return this.httpClient.post<Category>(this.baseUrl+"/save",category);
  }

  updateCategory(category:Category) : Observable<any>{
    return this.httpClient.put<Category>(this.baseUrl+"/update",category);
  }

  deleteCategory(idCategory:Number){
    return this.httpClient.delete(this.baseUrl+"/delete/"+idCategory);
  }
}
