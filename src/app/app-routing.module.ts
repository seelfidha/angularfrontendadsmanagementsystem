import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './components/content/category/category/category.component';
import { PaymentModeComponent } from './components/content/paymentMode/payment-mode/payment-mode.component';
import { AdComponent } from './components/content/ad/ad/ad.component';
import { AdFormComponent } from './components/content/ad/ad-form/ad-form.component';
import { AdListComponent } from './components/content/ad/ad-list/ad-list.component';


const routes: Routes = [
  {path:'categories',component: CategoryComponent},
  {path:'paymentModes',component:PaymentModeComponent},
  {path:'ads',component:AdListComponent},
  {path:'newAd',component:AdFormComponent},
  {path:'viewAd',component:AdComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
