import { PaymentMode } from '../paymentMode/payment-mode';
import { Category } from '../category/category';

export class Ad {

    idAd : number;
    name: string;
    description : number;
    fromAdress : string;
    toAdress : string; 
    departureDate : Date;
    arrivalDate : Date;
    payable : boolean;
    price : number;
    paymentMode : PaymentMode;
    category : Category;
}
