import { Component, OnInit } from '@angular/core';
import { PaymentModeService } from 'src/app/services/paymentMode/payment-mode.service';
import { PaymentMode } from 'src/app/entities/paymentMode/payment-mode';
import { FormBuilder,Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-payment-mode',
  templateUrl: './payment-mode.component.html',
  styleUrls: ['./payment-mode.component.css']
})
export class PaymentModeComponent implements OnInit {

  paymentModes : PaymentMode[] = [];
  paymentModeForm : FormGroup;
  selectedPaymentMode : PaymentMode = new PaymentMode();

  constructor(private paymentModeService : PaymentModeService, private formBuilder : FormBuilder) { 
    this.resetForm();
  }


  ngOnInit() {
    this.findAllPaymentModes();
  }

  findAllPaymentModes(){
    this.paymentModeService.findAllPaymentModes()
                           .subscribe(
                             res => {
                               this.paymentModes = res,
                               console.log(res)
                             },
                             error => console.log("error fetching payment modes "+error)
                             
                           );
  }

  editPaymentMode(paymentMode:PaymentMode){
    this.selectedPaymentMode = paymentMode;
  }

  deletePaymentMode(idPaymentMode : number){
    this.paymentModeService.deletePaymentMode(idPaymentMode)
                           .subscribe(
                             res => {
                               this.findAllPaymentModes(),
                               this.resetForm()
                            },
                            error => console.log("error deleting payment mode "+error)
                           );
  }

  savePaymentMode(){
    const paymentMode = this.paymentModeForm.value;
    if(paymentMode.idPayment == null){
      this.paymentModeService.savePaymentMode(paymentMode)
                             .subscribe(
                               response => {
                                 this.findAllPaymentModes(),
                                 this.resetForm()
                               },
                               error => console.log("error saving payment mode "+error)
                             );
    }else{
      this.paymentModeService.updatePaymentMode(paymentMode)
                             .subscribe(
                               response => {
                                 this.findAllPaymentModes(),
                                 this.resetForm()
                               },
                               error => console.log("error updating payment mode "+error)
                             );
    }
  }

  resetForm(){
    this.paymentModeForm = this.formBuilder.group({
      idPayment : null,
      label:['',Validators.required],
      description:''
    });
  }

}
