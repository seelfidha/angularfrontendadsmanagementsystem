import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category/category.service';
import { Category } from 'src/app/entities/category/category';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categories : Category[] = [];

  categoryForm:FormGroup;

  action:string;

  constructor(private categoryService:CategoryService, private formBuilder:FormBuilder) { 
    this.resetForm();
  }

  ngOnInit() {
    this.findAllCategories();
    this.action = 'New';
  }

  findAllCategories(){
    this.categoryService.findAllCategories()
                        .subscribe(
                          response => {
                            this.categories = response
                          },
                          error=> console.log("error fetching categories : "+error)
                        );
  }

  editCategory(category:Category){
    console.log("edit category"+category.idCategory);
    this.action = 'Edit '
      this.categoryForm = this.formBuilder.group({
        idCategory :category.idCategory,
        label:category.label,
        description:category.description
      })
  }

  deleteCategory(idCategory:number){
    console.log("delete category");
    this.categoryService.deleteCategory(idCategory)
                        .subscribe(
                          res => this.findAllCategories()
                        );
  }

  saveCategory(){
    const category = this.categoryForm.value;
    if(category.idCategory == null){
      this.categoryService.saveCategory(category)
                          .subscribe(
                            res => {
                              this.findAllCategories(),
                              this.resetForm()
                            }
                          );

    }else{
      this.categoryService.updateCategory(category)
                          .subscribe(
                            res => {
                              this.findAllCategories(),
                              this.resetForm()
                            }
                          )
    }
  }

  resetForm(){
    this.categoryForm = this.formBuilder.group({
      idCategory:null,
      label:['',Validators.required],
      description:''
    });
  }

}
