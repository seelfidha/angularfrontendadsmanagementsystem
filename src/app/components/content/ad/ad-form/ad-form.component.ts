import { Component, OnInit } from '@angular/core';
import { AdService } from 'src/app/services/ad/ad.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Ad } from 'src/app/entities/ad/ad';
import { CategoryService } from 'src/app/services/category/category.service';
import { Category } from 'src/app/entities/category/category';
import { PaymentModeService } from 'src/app/services/paymentMode/payment-mode.service';
import { PaymentMode } from 'src/app/entities/paymentMode/payment-mode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ad-form',
  templateUrl: './ad-form.component.html',
  styleUrls: ['./ad-form.component.css']
})
export class AdFormComponent implements OnInit {

  adForm : FormGroup;
  categories : Category[];
  paymentModes :PaymentMode[];

  constructor(private adService:AdService, 
              private categoryService:CategoryService, 
              private paymentModeService:PaymentModeService, 
              private formBuilder:FormBuilder, 
              private router:Router) { 


    var ad:Ad = new Ad();
    this.categoryService.findAllCategories()
                        .subscribe(
                          res => {
                            this.categories = res,
                            console.log(res)
                          },
                          error => console.log("error fetching categories "+error)
                        );
    this.paymentModeService.findAllPaymentModes()
                           .subscribe(
                             res => {
                               this.paymentModes = res,
                               console.log(res)
                             },
                             error => console.log("error fetching payment modes "+error)
                           )
    this.fillForm(ad);

  }

  ngOnInit() {
  }

  fillForm(ad:Ad){
    this.adForm = this.formBuilder.group({
      idAd:ad.idAd,
      name:ad.name,
      description:ad.description,
      fromAdress:ad.fromAdress,
      toAdress:ad.toAdress,
      departureDate:ad.departureDate,
      arrivalDate:ad.arrivalDate,
      price:ad.price,
      paymentMode:ad.paymentMode,
      category:ad.category
    })
  }

  saveAd(){
    var ad : Ad = this.adForm.value;
    var paymentMdode = ad.category;
    console.log("payment mode "+paymentMdode);
     if(ad.idAd != null){
        
     }else{
       console.log("save button clicked");
      

       this.adService.saveAd(ad)
                     .subscribe(
                       resp => this.router.navigate(['/ads'])
                     );
     }
  }

}
