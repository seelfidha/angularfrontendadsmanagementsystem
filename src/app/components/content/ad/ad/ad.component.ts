import { Component, OnInit } from '@angular/core';
import { AdService } from 'src/app/services/ad/ad.service';
import { Ad } from 'src/app/entities/ad/ad';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.css']
})
export class AdComponent implements OnInit {

  ad:Ad;

  constructor(private adService:AdService, private router:Router) {
    this.ad = this.adService.getSelectedAd();
   }

  ngOnInit() {
    console.log(this.ad);
    if(typeof this.ad === "undefined"){
      this.router.navigate(['/ads']);
    }
  }


}
