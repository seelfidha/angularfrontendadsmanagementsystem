import { Component, OnInit } from '@angular/core';
import { AdService } from 'src/app/services/ad/ad.service';
import { Ad } from 'src/app/entities/ad/ad';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.css']
})
export class AdListComponent implements OnInit {

 
  ads: Ad [] = [];

  constructor(private adService:AdService, private router:Router) { 
    this.findAllAd();
  }

  ngOnInit() {
  }

  findAllAd(){
    return this.adService.findAllAds()
                         .subscribe(
                            res => {
                              this.ads = res
                            },
                            error => console.log("error fetching ads : "+error)
                          );
  }

  viewAd(ad:Ad){
    this.adService.setSelectedAd(ad);
    this.router.navigate(['/viewAd']);
  }

}
